# SE

This repository hosts the companion code of the blog post on scripting
elixir applications with elixir hosted on [buguigny.org](http://buguigny.org)

Every branch hosts a specific test described in the post.

